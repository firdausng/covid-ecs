using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CovidEcs.Areas.Identity.Data.Seed;
using CovidEcs.Infrastructure.Data.Seed;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CovidEcs
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.GetService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetService<IdentitySeed>().EnsureSeedData().Wait();
                scope.ServiceProvider.GetService<DataSeed>().EnsureSeedData();
            }


            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CovidEcs.Infrastructure.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace CovidEcs.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext context;

        [BindProperty]
        public List<ViewModel> Vm { get; set; } = new List<ViewModel>();

        public IndexModel(ApplicationDbContext context)
        {
            this.context = context;
        }
        public async Task<IActionResult> OnGet()
        {
            var userId = Guid.Parse(User.Claims.FirstOrDefault(c => c.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")).Value);
            var supportDetails = await context.SupportDetails
                .Where(s => s.UserId.Equals(userId))
                .OrderByDescending(s => s.Title)
                .ToListAsync();

            Vm = supportDetails.Select(s => new ViewModel
            {
                RequestType = s.RequestType.ToString(),
                Status = s.Status.ToString(),
                Title = s.Title
            })
                .ToList();

            return Page();
        }
    }

    public class ViewModel
    {
        public string Status { get; set; }
        public string StatusReason { get; set; }
        public string Title { get; set; }
        public string RequestType { get; set; }
    }
}
